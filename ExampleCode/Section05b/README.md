This file contains compilation and running instructions for each of the test programs in this folder

5b01 - Assumes a valid integer as argv[1]
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b01.cpp -o 5b01
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b01 10

5b02_BF - Brute Force Solution to 5b.02 - Assumes a valid integer as argv[1]
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b02_BF.cpp -o 5b02_BF
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b02_BF 10

5b02_mon - Monenized Solution to 5b.02 - Assumes a valid integer as argv[1]
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b02_mon.cpp -o 5b02_mon
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b02_mon 10

5b03_BF - Brute Force Solution to Robot search problem
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b03_BF.cpp -o 5b03_BF
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b03_BF

5b03_hash - Hash Solution to Robot search problem
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b03_hash.cpp -o 5b03_hash
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b03_hash

5b04 - Recursive Solution to Towers of Hanoi
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b04.cpp -o 5b04
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b04

5b05 - Given a directed graph, find a route between two nodes
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b05.cpp -o 5b05
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b05

5b06 - 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b06.cpp -o 5b06
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b06

5b07 - Given a sorted (increasing order) array with unique integer elements, write an algorithm to create a BST with
minimal height
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b07.cpp -o 5b07
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b07

5b08 - T1 and T2 are two very large binary trees. with T1 much bigger than T2. Create an algorithm to determine if T2 is 
a subtree of T1
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b08.cpp -o 5b08
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b08

5b10 - A BST was created by traversing through an array from left to right, and inserting each element. Given a BST
with distinct elements, print all the possible arrays that could have led to this tree:

Example:
  2
 / \
1   3

Output: {2,1,3}; {2,3,1}

mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic 5b10.cpp -o 5b10
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ ./5b10

AVL Trees
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic AVLTest.cpp -o AVLTest

Binary Heap Test
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic BinaryHeapTest.cpp -o BinaryHeapTest

Binary Search Tree Test
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic BSTTest.cpp -o BSTTest

Cuckoo Hash Table Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic CuckooHashTableTest.cpp CuckooHashTable.cpp -o CuckooHashTableTest

Map Demo
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic mapDemo.cpp -o mapDemo

Priority Queue Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic PriorityQueueTest.cpp -o PriorityQueueTest

Quadratic Probing Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic QuadraticProbingTest.cpp QuadraticProbing.cpp -o QuadraticProbingTest

Red-Black Tree Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic RBTTest.cpp -o RBTTest

Separate Chaining Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic SeparateChainingTest.cpp SeparateChaining.cpp -o SeparateChainingTest

Sorting Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic SortTest.cpp -o SortTest

Splay Tree Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic SplayTest.cpp -o SplayTest

Treap Test 
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic TreapTest.cpp -o TreapTest

Vector Test
mmorri22@remote304:~/CSE21312/CTCI/Recursion$ g++ -std=c++11 -Wpedantic VectorTest.cpp -o VectorTest



