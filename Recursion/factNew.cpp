/**********************************************
* File: factNew.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* Runs the factorial solver with the  
**********************************************/
#include "Supp.h"
#include "Recurse.h"

/********************************************
* Function Name  : factorial
* Pre-conditions : unsigned int i
* Post-conditions: unsigned int
*  
* Calculates i!
********************************************/
/*unsigned int factorial(unsigned int i){
	
	// Error Case 
	if(i < 0){
		std::cout << "Please enter a positive number" << std::endl;
		exit(-1);
	}
	
	std::cout << "Recursive call for " << i << std::endl;
	
	// Base case i = 0
	if(i == 0){
		std::cout << "Base Case found!" << std::endl;
		std::cout << "Base Case Result: " << i << std::endl;
		return 1;
	}
	
	// Recursive case
	int ret = i* factorial(i-1);
	
	// Printed to show students
	std::cout << "Recursive Result: " << ret << std::endl;
	return ret;
}*/

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* Main driver for factorial fundamental
* Requires a valid positive integer as argv[1]
********************************************/
int main(int argc, char** argv){

	// Call the recursive function
	std::cout << "Recursive Final Result: " << factorial(getArgv1Num(argc, argv)) << std::endl;

	return 0;

}
